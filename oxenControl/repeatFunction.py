import completeFunction

def repeatOxenControl():
    while True:
        repeat = input("Digite 'c' para continuar e fazer outra operação ou 's' para sair: ")
        if (repeat == "c"):
            repeatOxenControl = completeFunction.oxenControl()
        elif (repeat == "s"):
            print("A operação acabou")
            break
        else:
            print("Não entendi. Por favor, digite apenas 'c' ou 's'")
