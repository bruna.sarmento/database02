import json

def maxOxen():
    with open('dataOxen.json') as file:
        dataOx = json.load(file)
        maxOx = max(dataOx['dataOxen'], key=lambda key: key['weight'])
        print(maxOx)