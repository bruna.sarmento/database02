import json

def getDataOxen():
    data = {}
    data['id'] = str(input('insira o código do boi: '))
    data['name'] = str(input('insira o nome do boi: '))
    data['weight'] = float(input('insira o peso do boi (kg): '))
    return (data)

def addDataJson(start, filename ='dataOxen.json'):
    with open(filename,'r+') as file:
        dataOx = json.load(file)
        dataOx["dataOxen"].append(start)
        file.seek(0)
        json.dump(dataOx, file, indent = 3)
        return dataOx
