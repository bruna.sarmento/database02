import json

def minOxen():
    with open('dataOxen.json') as file:
        dataOx = json.load(file)
        minOx = min(dataOx['dataOxen'], key=lambda key: key['weight'])
        print(minOx)