import json
import menu
import addDataOxen
import maxWeightOxen
import minWeightOxen

def oxenControl():
    menu1 = menu.printMenu()
    startMenu = int(input("Digite a opção desejada: "))

    if (startMenu == 1):
        start = addDataOxen.getDataOxen()
        jsonData = addDataOxen.addDataJson(start, filename ='dataOxen.json')
        print("Seus dados foram cadastrados")

    elif (startMenu == 2):
        fatOx = maxWeightOxen.maxOxen()

    elif (startMenu == 3):
        leanOx = minWeightOxen.minOxen()

    else:
        print("Por favor, digite apenas uma das opções citadas")